# 常用命令

## 配置
```
git config --global user.name "Your Name"
git config --global user.email "email@example.com"
```

### 创建版本库

```
git init
```

## 本地提交

```
git add file(文件名) [.代表所有文件]
git commit -m "提交记录提示"
```

## 查看状态

```
git status
```

## 比较文件

```
git diff file(文件名)
```

## 查看提交记录

```
git log
```

## 回退版本

```
git reset --hard version(版本号)
```

## 丢弃修改的文件

```
git checkout -- file(文件名)
git rm 将文件从缓存区移除
```

# 远程配置

## 添加ssh 

1. 创建SSH Key。在用户主目录下，看看有没有.ssh目录，如果有，再看看这个目录下有没有id_rsa和id_rsa.pub这两个文件，如果已经有了，可直接跳到下一步。如果没有，打开Shell（Windows下打开Git Bash），创建SSH Key：

```
ssh-keygen -t rsa -C "youremail@example.com"
```

可以在用户主目录里找到.ssh目录，里面有id_rsa和id_rsa.pub两个文件，这两个就是SSH Key的秘钥对，id_rsa是私钥，不能泄露出去，id_rsa.pub是公钥

2. 登陆GitHub，打开“Account settings”，“SSH Keys”页面, 点“Add SSH Key”，填上任意Title，在Key文本框里粘贴id_rsa.pub文件的内容

## 远程库

```
git remote add origin git@server-name:path/repo-name.git
git remote rm origin 删除远程库
git remote -v 查看远程库信息
```

## 推送远程库

```
git push -u origin master 第一次给远程推送
git push 
```

## 同步远程库

```
git pull 从远端仓库提取数据并尝试合并到当前分支
git fetch 从远端仓库下载新分支与数据
```

## 从远程库克隆

```
git clone git@server-name:path/repo-name.git
git clone https://@server-name:path/repo-name.git
```

## 分支

```
git branch 分支名 创建分支
git checkout 分支名 切换分支
git switch 分支名 切换分支
git checkout -b 分支名 创建并切换分支
it switch -c 分支名 创建并切换分支
git branch 查看分支
git merge 分支名 将分支合并到当前分支
git branch -d  分支名 删除分支
```

## 标签
```
git tag <tagname> 创建标签
git tag 查看所有标签
git show <tagname> 查看标签信息
git tag -d <tagname> 删除标签
git push origin <tagname>  将指定的标签推送到远程
git push origin --tags 将所有的标签推送到远程
git push origin :refs/tags/<tagname> 删除一个远程标签 
```

## 暂存区
```
git stash save "暂存的备注"  暂存
git stash list 查看暂存记录
git stash pop 取回暂存代码
git stash clear 清空暂存
```
