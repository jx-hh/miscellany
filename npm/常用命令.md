# 变量说明

```
<name> 模块名
```
## npm 基本命令
```
npm config set registry https://registry.npm.taobao.org --global 设置镜像源
npm config get registry 看镜像源
npm init 创建模块
npm -v 查看npm的版本 
npm -l 查看npm命令列表
npm help 查看npm的用法或这个指令的说明
npm config list -l  查看 npm 的配置
npm login 登录npm官网
npm adduser 新增用户
npm whoami 查看当前是谁在登录npm
npm publish 发布模块
npm docs <name> 查看包的文档
npm home <name> 查看包的首页/官网
npm search <name> 根据关键字查找包
npm list 当前项目安装的所有模块
npm install  安装依赖的所有模块
npm install <name> 安装依赖的指定模块
npm update  升级依赖的所有模块
npm update <name> 升级依赖的指定模块
npm run 执行脚本 package.json的scripts字段
```